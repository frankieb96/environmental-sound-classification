import tensorflow as tf

"""
def create_ESCmodel_ConvDirect(input_mfcc_shape, input_zcr_shape, output_size, name='ConvDirect'):
    
    Create a keras model for Environmental Sound Classification. This uses a convolutional approach to MFCC coefficients.
    
    :param input_mfcc_shape: shape of the MFCC input vector
    :param input_zcr_shape: shape of the ZCR input vector
    :param output_size: number of classes in data (just for smoke test purposes)
    :return: the keras model
    

    # prefix 'M_' stands for the MFCC
    # prefix 'Z_' stands for the ZCR
    # prefix 'C_' is after the concatenation

    # input -> conv -> maxpool -> flatten -> dense -> batchnorm -> ReLU

    M_input = tf.keras.Input(input_mfcc_shape, name="Input_MFCC")
    M_layer = tf.keras.layers.Conv2D(filters=32, kernel_size=(10,2), strides=(2,1), padding="same", name="conv2d_1_MFCC")(M_input) # no padding
    M_layer = tf.keras.layers.BatchNormalization()(M_layer)
    M_layer = tf.keras.layers.Activation('relu')(M_layer)
    M_layer = tf.keras.layers.MaxPool2D(pool_size=(10,1), strides=(5,1))(M_layer)
    M_layer = tf.keras.layers.Flatten()(M_layer)

    Z_input = tf.keras.Input(input_zcr_shape, name="Input_ZCR")
    Z_layer = tf.keras.layers.Flatten(name="flatten_1_ZCR")(Z_input)
    Z_layer = tf.keras.layers.Dense(units=16, name="dense_1_ZCR")(Z_layer)
    Z_layer = tf.keras.layers.BatchNormalization(name="batchnorm_1_MFCC")(Z_layer)
    Z_layer = tf.keras.layers.Activation('relu', name="activation_1_ZCR")(Z_layer)

    C_layer = tf.keras.layers.concatenate([M_layer, Z_layer], name="concatenate_1")
    C_layer = tf.keras.layers.Flatten(name="flatten_1")(C_layer)
    C_layer = tf.keras.layers.Dense(units=output_size, name="dense_final")(C_layer)
    #C_layer = tf.keras.layers.BatchNormalization(name="batchnorm_1")(C_layer)
    C_layer = tf.keras.layers.Activation('softmax', name="activation_1_c")(C_layer)

    model = tf.keras.models.Model(inputs=[M_input, Z_input], outputs=C_layer, name=name)
    return model
# END OF create_ESCmodel_ConvDirect
"""


def create_ESCmodel_ConvDirect(input_mfcc_shape, input_zcr_shape, output_size, model_name='ConvDirect'):
    M_input = tf.keras.Input(input_mfcc_shape, name="Input_MFCC")
    M_layers = tf.keras.layers.Flatten()(M_input)

    Z_input = tf.keras.Input(input_zcr_shape, name="Input_ZCR")
    Z_layers = tf.keras.layers.Flatten()(Z_input)

    C_layers = tf.keras.layers.Concatenate(name="concatenate")([M_layers, Z_layers])
    C_layers = tf.keras.layers.Dense(units=output_size, activation='softmax', name='output')(C_layers)

    model = tf.keras.models.Model(inputs=[M_input, Z_input], outputs=C_layers, name=model_name)
    return model
