"""
Project number 5 for the Human Data Analytics course, year 2019-2020, Università degli Studi di Padova.
# TODO fix here
ESC-process.py program. It does the following:
It expects as command line argument input the path to the ESC 50 master folder.
It outputs 'data_array.npz' file that contains the zipped arrays.

Author: Francesco Bianco

Version: 0.9

Email: francesco.bianco.5@studenti.unipd.it

Status: Working, Waiting for next development
"""
import numpy as np
import sklearn as sk


def normalize(array, axis=0, type='standard'):
    """
    Normalize the array between [0,1] in type='standard', or [-1,1] for 'symmetric', along the given axis.

    :param array: numpy.array type to be normalized
    :param axis: axis along which to normalize
    :param type: 'standard' -> normalize in [0,1], 'symmetric' -> normalize in [-1,1]
    :return: the normalized array
    """
    if type not in ['standard', 'symmetric']:
        raise ValueError("Type must either be 'symmetric' or 'standard'")
    max = array.max(axis=axis, keepdims=True)
    min = array.min(axis=axis, keepdims=True)
    if type == 'symmetric':  # [-1,1]
        array = 2 * ((array - min) / (max - min)) - 1
    else:  # standard [0,1]
        array = (array - min) / (max - min)
    return array


def split_training_validation_test_sets(mfcc_array, zcr_array, labels, train_ratio=0.75, validation_ratio=0.15,
                                        test_ratio=0.10, random_state=1):
    """
    Split the dataset into training, validation, test sets based on ratio. It's a commodity that wraps
    sklearn.model_selection.train_test_split() and ensures that the labels are coherent among mfcc and
    zcr sets, so it always shuffles with the selected random state.
    Inputs and outputs are self-explanatory.

    :param mfcc_array:
    :param zcr_array:
    :param labels:
    :param train_ratio:
    :param validation_ratio:
    :param test_ratio:
    :param random_state:
    :return: mfcc_train, zcr_train, y_train, mfcc_val, zcr_val, y_val, mfcc_test, zcr_test, y_test
    """
    mfcc_train, mfcc_test, y_train_m, y_test_m = sk.model_selection.train_test_split(mfcc_array, labels,
                                                                                     test_size=1 - train_ratio,
                                                                                     shuffle=True,
                                                                                     random_state=random_state)
    zcr_train, zcr_test, y_train_z, y_test_z = sk.model_selection.train_test_split(zcr_array, labels,
                                                                                   test_size=1 - train_ratio,
                                                                                   shuffle=True,
                                                                                   random_state=random_state)
    y_train = y_train_z
    y_test = y_test_z
    # sanity check 1 and 2
    if not np.array(y_train_m == y_train_z).all():
        raise ValueError("ERROR ON SANITY CHECK (1): y_train_m != y_train_z")
    if not np.array(y_train_m == y_train_z).all():
        raise ValueError("ERROR ON SANITY CHECK (2): y_train_m != y_train_z")
    # test is now 10% of the initial data set
    # validation is now 15% of the initial data set
    mfcc_val, mfcc_test, y_val1, y_test1 = sk.model_selection.train_test_split(mfcc_test, y_test,
                                                                               test_size=test_ratio / (
                                                                                       test_ratio + validation_ratio),
                                                                               shuffle=True, random_state=random_state)
    zcr_val, zcr_test, y_val2, y_test2 = sk.model_selection.train_test_split(zcr_test, y_test, test_size=test_ratio / (
            test_ratio + validation_ratio), shuffle=True, random_state=random_state)
    y_val = y_val2
    y_test = y_test2
    # sanity check 3 and 4
    if not np.array(y_val1 == y_val2).all():
        raise ValueError("ERROR ON SANITY CHECK (3): y_val1 != y_val2")
    if not np.array(y_test1 == y_test2).all():
        raise ValueError("ERROR ON SANITY CHECK (4): y_test1 != y_test2")

    return mfcc_train, zcr_train, y_train, mfcc_val, zcr_val, y_val, mfcc_test, zcr_test, y_test
