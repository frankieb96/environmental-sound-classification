"""
Project number 5 for the Human Data Analytics course, year 2019-2020, Università degli Studi di Padova.

ESC-preprocess.py program. It does the following:
 - loads the dataset, polishes it by trimming/overlaying to bring exactly 5 seconds long audio,
 - augment data through noise addition, pitch shifting, time shifting, time stretching
 - computes mfcc and zcr coefficients, and associated labels, as numpy's vectors
 - saves such vectors to a single file
It expects as command line argument input the path to the ESC 50 master folder.
It outputs 'data_array.npz' file that contains the zipped arrays.

 Author: Francesco Bianco

 Version: 0.9

 Email: francesco.bianco.5@studenti.unipd.it

 Status: Working, Waiting for next development
"""

import os
import time
import librosa.display
import librosa
import numpy as np
import pandas as pd
import sys
import random
try:
    from tabulate import tabulate
except ImportError:
    print("WARNING: can't import 'tabulate' module. Running without.")

random.seed(1)

print("\n### Program ESC_preprocess has started.")

if (len(sys.argv) < 2):
    raise ValueError(
        "Not enough parameters passed in input. Usage: <path-to-ESC-master> [optional parameter -s]")

""" GLOBAL VARIABLES AND CONSTANTS """
DATASET_NAME = 'ESC-50'
AUDIO_PATH = str(sys.argv[1]) + '/audio/'  # audio path
CSV_PATH = str(sys.argv[1]) + '/meta/'  # metadata path
SMOKE_TEST = ('-s' in sys.argv)  # smoke test settings
SMOKE_NUM = 10

SAMPLING_RATE = 44100  # all records are sampled at 44.1 kHz
AUDIO_LENGTH = 220500  # audio length in samples
MFCC_N = 13  # number of MFCC coeff
FFT_WIN_N = 431  # number of FFT windows (from librosa default values, for this specific problem 5 sec audio)
TOTAL_AUGMENTATIONS_PER_SAMPLE = 4  # total number of augmentations (0 -> original only, 1 -> noisy, 4 -> all)
TOTAL_ALLOCATION_ROWS = 2000 * (1 + TOTAL_AUGMENTATIONS_PER_SAMPLE)  # to preallocate dataset
if (SMOKE_TEST):
    TOTAL_ALLOCATION_ROWS = SMOKE_NUM * (1 + TOTAL_AUGMENTATIONS_PER_SAMPLE)

# check over the constants
if not os.path.exists(AUDIO_PATH):
    raise ValueError("Can't find audio path " + AUDIO_PATH)
if not os.path.exists(CSV_PATH):
    raise ValueError("Can't find metadata path " + CSV_PATH)
if (SMOKE_TEST):
    print("ARG '-s' -> PROGRAM WILL RUN ON SMOKE DATA")
print("")

""" LOAD DATASET ESC-50 """
print("Loading metadata...", end=' ')
esc50_pdframe = pd.read_csv(CSV_PATH + '/esc50.csv')
# if smoke, take just SMOKE_NUM samples
if (SMOKE_TEST):
    print("SMOKE TEST: loading ", SMOKE_NUM, " elements...", end=' ')
    esc50_pdframe = esc50_pdframe[0:SMOKE_NUM]
print("done. Total categories: ", esc50_pdframe['target'].nunique(), ", dataset length: ", len(esc50_pdframe))

""" CREATE DATA AUGMENTATION FUNCTIONS """
print("Building the data augmentation functions...", end=' ')
# build the lambdas for data augmentation
raw_lambda = lambda el: el
noise_lambda = lambda el: el + 0.009 * np.random.normal(0, 1, len(el)).astype(np.float32)
shift_lambda = lambda el: np.roll(el, int(SAMPLING_RATE / 2))
stretch_lambda = lambda el: librosa.effects.time_stretch(y=el, rate=0.9)[0:AUDIO_LENGTH]
pitch_lambda = lambda el: librosa.effects.pitch_shift(y=el, sr=SAMPLING_RATE, n_steps=-1.0)
audio_augmentation = [raw_lambda, noise_lambda, shift_lambda, stretch_lambda, pitch_lambda]
print("done.")

""" APPLY DATA AUGMENTATION AND GET THE FEATURES """
print("Building the dataset...", end=' ')
start = time.time()

# since DataFrame.append() is inefficient, we preallocate the dataframe dimension
mfcc_array = np.zeros((TOTAL_ALLOCATION_ROWS, FFT_WIN_N, MFCC_N), dtype=np.float32)  # (?, 431, 13)
zcr_array = np.zeros((TOTAL_ALLOCATION_ROWS, FFT_WIN_N, 1), dtype=np.float32)  # (?, 431, 1)
labels = np.zeros(TOTAL_ALLOCATION_ROWS, dtype=np.int8)

data_index = -1  # data index is the index of the feature arrays, which are preallocated
for index, row in esc50_pdframe.iterrows():
    filename = row['filename']
    target = row['target']

    raw = librosa.core.load(AUDIO_PATH + filename, sr=SAMPLING_RATE)[0]  # NOTE: this is a 'tuple' object, data in [0]
    # recordings are sometimes not frame accurate, so we trim/overlay to exactly 5 seconds
    if (len(raw) > AUDIO_LENGTH):
        raw = raw[:AUDIO_LENGTH]
    elif (len(raw) < AUDIO_LENGTH):
        raw = np.pad(raw, (0, max(0, AUDIO_LENGTH - len(raw))), "constant")  # pad with zeros to the right

    for j in range(0, TOTAL_AUGMENTATIONS_PER_SAMPLE):  # note that for j=0 it returns the raw signal
        data_index = data_index + 1
        elem = audio_augmentation[j](raw)  # call to lambdas
        # get feature
        # MFCC computation with default settings (2048 FFT window length, 512 hop length, 128 bands)
        zcr = librosa.feature.zero_crossing_rate(elem).T  # .T for column vector
        mfcc = librosa.feature.mfcc(y=elem, sr=SAMPLING_RATE, n_mfcc=13).T  # shape (431, 13)
        # save features
        mfcc_array[data_index] = mfcc
        zcr_array[data_index] = zcr
        labels[data_index] = target
# END of 'for index, row in esc50_pdframe.iterrows()':
end = time.time()
print("done. Total time needed: ", "{:.2f}".format((end - start) / 60),
      "minutes.")  # print("applying data transformation...)

# shuffling dataset (we need to make sure that the transformation are coherent
print("Shuffling data...", end=' ')
np.random.seed(1)
permutation = np.random.permutation(TOTAL_ALLOCATION_ROWS)
mfcc_array = mfcc_array[permutation]
zcr_array = zcr_array[permutation]
labels = labels[permutation]
print("done.")

print("Dataset memory footprint:")
# see memory usage
if 'tabulate' in sys.modules:
    print("")
    headers = ["", "shape", "data type", "bytes", "Megabytes"]
    table = [["MFCC feature array", mfcc_array.shape, mfcc_array.dtype, mfcc_array.nbytes, "{:.2f}".format(mfcc_array.nbytes / (1024 ** 2))],
             ["ZCR feature array", zcr_array.shape, zcr_array.dtype, zcr_array.nbytes, "{:.2f}".format(zcr_array.nbytes / (1024 ** 2))],
             ["labels array", labels.shape, labels.dtype, labels.nbytes, "{:.2f}".format(zcr_array.nbytes / (1024 ** 2))]]
    print(tabulate(table, headers=headers))
    print("")
else:
    print(" - MFCC features array: shape", mfcc_array.shape, "- total ", mfcc_array.nbytes, " bytes (",
          "{:.2f}".format(mfcc_array.nbytes / (1024 ** 2)), " MB ) - data type \'", mfcc_array.dtype, "\'")
    print(" - ZCR features array: shape", zcr_array.shape, "- total ", zcr_array.nbytes, " bytes (",
          "{:.2f}".format(zcr_array.nbytes / (1024 ** 2)), " MB ) - data type \'", zcr_array.dtype, "\'")
    print(" - labels array: shape", labels.shape, "- total ", labels.nbytes, " bytes (",
          "{:.2f}".format(labels.nbytes / (1024 ** 2)), " MB ) - data type \'", labels.dtype, "\'")

# save the transformed dataset
print("Saving the transformed dataset...", end=' ')
if (SMOKE_TEST):
    np.savez_compressed("smoke_data_arrays", mfcc=mfcc_array, zcr=zcr_array, labels=labels)
else:
    np.savez_compressed("data_arrays", mfcc=mfcc_array, zcr=zcr_array, labels=labels)
print("done.")

print("\n### Program ESC_preprocess terminated.")
