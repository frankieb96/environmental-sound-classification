"""
Project number 5 for the Human Data Analytics course, year 2019-2020, Università degli Studi di Padova.
# TODO fix here
ESC-process.py program. It does the following:
It expects as command line argument input the path to the ESC 50 master folder.
It outputs 'data_array.npz' file that contains the zipped arrays.

Author: Francesco Bianco

Version: 0.9

Email: francesco.bianco.5@studenti.unipd.it

Status: Working, Waiting for next development
"""

import numpy as np
import pandas as pd
import sys
import sklearn as sk
from sklearn import model_selection
import tensorflow as tf
import ESC_utils
import ESC_models
from matplotlib import pyplot as plt

try:
    from tabulate import tabulate
except ImportError:
    print("WARNING: can't import 'tabulate' module. Running without.")

"""
print(mfcc_array.shape)
print(mfcc_array.dtype)
print(mfcc_array)
"""

print("\n### Program ESC_process has started.")

if len(sys.argv) < 2:
    raise ValueError(
        "Not enough parameters passed in input. Usage: <path-to-data-file> [symmetric | standard]")

if sys.argv[2] not in ['symmetric', 'standard']:
    raise ValueError(
        "Wrong parameter \'" + sys.argv[2] + "\' passed. Acceptable parameters: [symmetric | standard]")

""" GLOBAL VARIABLES AND CONSTANTS """
DATA_PATH = sys.argv[1]
NORMALIZATION_TYPE = sys.argv[2]

""" LOAD DATA """
print("Loading feature arrays...", end=' ')
with np.load(DATA_PATH) as load:
    mfcc_array = load['mfcc']
    zcr_array = load['zcr']
    labels = load['labels']
print("done.")

""" NORMALIZE DATA """
print("Nomalizing data...", end=' ')
# mfcc: axis=0 for global, axis=1 for each mfcc spectrogram (431x13), axis=(1,2) for each mfcc vector(1x13)
mfcc_array = ESC_utils.normalize(mfcc_array, axis=0, type=NORMALIZATION_TYPE)
# zcr
zcr_array = ESC_utils.normalize(zcr_array, axis=0, type=NORMALIZATION_TYPE)
print("done. Normalization type: ", NORMALIZATION_TYPE)  # print("Normalizing data...",end=' ')

""" RESHAPING FOR CONV LAYERS """
# TODO reshaping for conv layers

""" GET TRAINING / VALIDATION / TEST SETS """
print("Creating training / validation / test sets...", end=' ')
train_ratio = 0.75
validation_ratio = 0.15
test_ratio = 0.10
# train is now 75% of the entire data set
mfcc_train, zcr_train, y_train, mfcc_val, zcr_val, y_val, mfcc_test, zcr_test, y_test = ESC_utils.split_training_validation_test_sets(
    mfcc_array, zcr_array, labels, train_ratio, validation_ratio, test_ratio, random_state=1)
print("done. Dimensions are:")
if 'tabulate' in sys.modules:
    print("")
    headers = ["", "MFCC array", "ZCR array", "Labels array"]
    table = [["Training set", mfcc_train.shape, zcr_train.shape, y_train.shape],
             ["Validation set", mfcc_val.shape, zcr_val.shape, y_val.shape],
             ["Test set", mfcc_test.shape, zcr_test.shape, y_test.shape]]
    print(tabulate(table, headers=headers))
else:
    print(" - Training set: MFCC", mfcc_train.shape, "+ ZCR", zcr_train.shape, "+ labels", y_train.shape)
    print(" - Validation set: MFCC", mfcc_val.shape, "+ ZCR", zcr_val.shape, "+ labels", y_val.shape)
    print(" - Test set: MFCC", mfcc_test.shape, "+ ZCR", zcr_test.shape, "+ labels", y_test.shape)

# now we have the following:
# training sets: mfcc_train, zcr_train, y_train
# validation sets: mfcc_val, zcr_val, y_val
# test sets: mfcc_test, zcr_test, y_test
mfcc_input_shape = (mfcc_train.shape[1], mfcc_train.shape[2])
zcr_input_shape = (zcr_train.shape[1], zcr_train.shape[2])
model_ConvDirect = ESC_models.create_ESCmodel_ConvDirect(mfcc_input_shape, zcr_input_shape, 50, model_name="ConvDirect")
model_ConvDirect.compile(optimizer='adam',
                         loss='sparse_categorical_crossentropy',
                         metrics=['sparse_categorical_accuracy']
                         )
model_ConvDirect.summary()
history = model_ConvDirect.fit(
    {"Input_MFCC": mfcc_train, "Input_ZCR": zcr_train},
    y_train,
    batch_size=32,
    epochs=10,
    verbose=1,
    validation_data=({"Input_MFCC": mfcc_val, "Input_ZCR": zcr_val}, y_val)
)
